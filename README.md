# PyHooker: A GitLab Web Hooks Manager

## Usage

Point your GitLab Web Hooks at: http://hooker.app.srv:8337

This program will map your repository and branch to the Jenkins CI build trigger.

For example when push is on some\_it\_project origin master then a trigger on some\_it\_project job will launch with token xmaster (the x is simple hash for now)

## TODO

Daemonize...
