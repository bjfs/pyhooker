#!/usr/bin/env python3
#
# PyHooker
#
# Author: Bartłomiej Stobiecki <bartek@stobiecki.eu>
#

import http.server
import socketserver
import urllib.request
import logging
import json

PORT = 8337
JURI = 'http://jenkins.app.srv/job/'
LOGFILE = 'hooker.log'
LOGFORMAT = '%(asctime)-15s %(message)s'

logging.basicConfig(filename=LOGFILE, format=LOGFORMAT, level=logging.INFO)

class ServerHandler(http.server.SimpleHTTPRequestHandler):
    def triggerHandler(self, repo, ref):
        triggerLink = JURI + repo + '/build?token=' + 'x' + ref.split('/')[2]  # when branch = foo then token = xfoo
        data = '{"make":"build"}'.encode('utf-8')  # content can be anything...
        header = {"Content-Type":"application/json"}
        try:
            req = urllib.request.Request(triggerLink, data, header)
            response = urllib.request.urlopen(req)
            logging.info("== Trigger")
            logging.info(triggerLink)
            logging.info("== CI response")
            logging.info(response.read().decode('utf-8'))
        except Exception as ex:
            logging.warning('** Request failed, details: %s', ex)

    def do_POST(self):
        logging.info("=== Incoming POST...")
        try:
            raw = self.rfile.read(int(self.headers.get('Content-Length')))
            data = json.loads(raw.decode("utf-8"))
            logging.info("== Project name")
            logging.info(data['repository']['name'])
            logging.info('== Ref');
            logging.info(data['ref'])
            self.triggerHandler(data['repository']['name'], data['ref'])
        except Exception as e:
            logging.warning('*** Something went wrong, details: %s', e)
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

httpd = socketserver.TCPServer(("", PORT), ServerHandler)

print("PyHooker is serving at port", PORT)
httpd.serve_forever()
